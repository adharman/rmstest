/* package com.harman.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;


import com.harman.web.configuration.HibernateConfiguration;
import com.harman.web.model.Device;
import com.harman.web.utils.TestUtil;

@RunWith(MockitoJUnitRunner.class)
public class CommonDAOImplTest {

	@InjectMocks
	private CommonDAOImpl commomDAOImpl;	
	
	@Mock
	private HibernateConfiguration hibernateConfiguration;
	@Mock
	private SessionFactory sessionFactory;
	@Mock
	private Session session;
	@Mock
	private Criteria criteria;
	@Mock 
	private Query query;
	
	TestUtil testUtil=null;
	
	
	
	@Before
	public void setup(){
		//commomDAOImpl=mock(CommonDAOImpl.class);
		 testUtil=new TestUtil();
	}
	
	@Test
	public void findAllDevicesTets() {
		List<Device> findProjector=null;	
		
		when(commomDAOImpl.getSession()).thenReturn(session);
		when(session.createQuery(Mockito.anyString())).thenReturn(query);		
		when(query.list()).thenReturn(findProjector);
		
		assertEquals(testUtil.getDevice(),findProjector);	
	}


	
	
}*/
