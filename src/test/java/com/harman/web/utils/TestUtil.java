package com.harman.web.utils;

import com.harman.web.model.Device;

public class TestUtil {
	
	public Device getDevice()
	{
		Device device=new Device();
		device.setCapacity(23);
		device.setDeviceid(1);
		device.setDevicetype("projector");
		device.setModelNo("G1234");
		device.setUsedHour(12);
		return device;
	}

}
