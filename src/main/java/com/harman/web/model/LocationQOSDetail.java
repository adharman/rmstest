package com.harman.web.model;

public class LocationQOSDetail implements Comparable<LocationQOSDetail> {
	
	private String locationGroup;
	
	private String locationName;
	
	private Integer locationID;
	
	private Integer summaryYear;
	
	private Integer summaryMonth;
	
	private Integer summaryDay;
	
	private Long downTime;
	
	private Long upTime;
	
	private Double QoS;
	
	private String locationGroupDescription;
	
	private String locationDescription;
	
	private String locationPrestigeName;

	private String locationTimezone;

	private Long locationPhoneNumber;

	public String getLocationGroup() {
		return locationGroup;
	}

	public LocationQOSDetail setLocationGroup(String locationGroup) {
		this.locationGroup = locationGroup;
		return this;
	}

	public String getLocationName() {
		return locationName;
	}

	public LocationQOSDetail setLocationName(String locationName) {
		this.locationName = locationName;
		return this;
	}

	public Integer getLocationID() {
		return locationID;
	}

	public LocationQOSDetail setLocationID(Integer locationID) {
		this.locationID = locationID;
		return this;
	}

	public Integer getSummaryYear() {
		return summaryYear;
	}

	public LocationQOSDetail setSummaryYear(Integer summaryYear) {
		this.summaryYear = summaryYear;
		return this;
	}

	public Integer getSummaryDay() {
		return summaryDay;
	}

	public LocationQOSDetail setSummaryDay(Integer summaryDay) {
		this.summaryDay = summaryDay;
		return this;
	}

	public Long getDownTime() {
		return downTime;
	}

	public LocationQOSDetail setDownTime(Long downTime) {
		this.downTime = downTime;
		return this;
	}

	public Long getUpTime() {
		return upTime;
	}

	public LocationQOSDetail setUpTime(Long upTime) {
		this.upTime = upTime;
		return this;
	}

	public Double getQoS() {
		return QoS;
	}

	public LocationQOSDetail setQoS(Double qoS) {
		QoS = qoS;
		return this;
	}

	public String getLocationGroupDescription() {
		return locationGroupDescription;
	}

	public LocationQOSDetail setLocationGroupDescription(String locationGroupDescription) {
		this.locationGroupDescription = locationGroupDescription;
		return this;
	}

	public String getLocationDescription() {
		return locationDescription;
	}

	public LocationQOSDetail setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
		return this;
	}

	public String getLocationPrestigeName() {
		return locationPrestigeName;
	}

	public LocationQOSDetail setLocationPrestigeName(String locationPrestigeName) {
		this.locationPrestigeName = locationPrestigeName;
		return this;
	}

	public String getLocationTimezone() {
		return locationTimezone;
	}

	public LocationQOSDetail setLocationTimezone(String locationTimezone) {
		this.locationTimezone = locationTimezone;
		return this;
	}

	public Long getLocationPhoneNumber() {
		return locationPhoneNumber;
	}

	public LocationQOSDetail setLocationPhoneNumber(Long locationPhoneNumber) {
		this.locationPhoneNumber = locationPhoneNumber;
		return this;
	}

	public Integer getSummaryMonth() {
		return summaryMonth;
	}

	public LocationQOSDetail setSummaryMonth(Integer summaryMonth) {
		this.summaryMonth = summaryMonth;
		return this;
	}
	
	@Override
	public String toString() {
		String locQOS = "uptime is: " + upTime + " downtime is : " + downTime + " Year is : " + summaryYear + 
						" QOS is  : " + QoS;
		return locQOS;
	}

	@Override
	public int compareTo(LocationQOSDetail locQOC) {
		return locQOC.summaryYear - this.summaryYear;
	}
	
}
