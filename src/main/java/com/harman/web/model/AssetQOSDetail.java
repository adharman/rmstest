package com.harman.web.model;

public class AssetQOSDetail {

	private String locationGroup;
	
	private String locationName;
	
	private Integer locationID;
	
	private String assetName;
	
	private Integer assetID;
	
	private Integer summaryYear;
	
	private Integer summaryMonth;
	
	private Integer summaryDay;
	
	private Long downTime;
	
	private Long upTime;
	
	private Double QoS;
	
	private String locationGroupDescription;
	
	private String locationDescription;
	
	private String locationPrestigeName;

	private String locationTimezone;

	private Long locationPhoneNumber;
	
	private String modelName;
	
	private String manufacturerName;
	
	private Integer assetTypeId;

	public String getLocationGroup() {
		return locationGroup;
	}

	public AssetQOSDetail setLocationGroup(String locationGroup) {
		this.locationGroup = locationGroup;
		return this;
	}

	public String getLocationName() {
		return locationName;
	}

	public AssetQOSDetail setLocationName(String locationName) {
		this.locationName = locationName;
		return this;
	}

	public Integer getLocationID() {
		return locationID;
	}

	public AssetQOSDetail setLocationID(Integer locationID) {
		this.locationID = locationID;
		return this;
	}

	public Integer getSummaryYear() {
		return summaryYear;
	}

	public AssetQOSDetail setSummaryYear(Integer summaryYear) {
		this.summaryYear = summaryYear;
		return this;
	}

	public Integer getSummaryDay() {
		return summaryDay;
	}

	public AssetQOSDetail setSummaryDay(Integer summaryDay) {
		this.summaryDay = summaryDay;
		return this;
	}

	public Long getDownTime() {
		return downTime;
	}

	public AssetQOSDetail setDownTime(Long downTime) {
		this.downTime = downTime;
		return this;
	}

	public Long getUpTime() {
		return upTime;
	}

	public AssetQOSDetail setUpTime(Long upTime) {
		this.upTime = upTime;
		return this;
	}

	public Double getQoS() {
		return QoS;
	}

	public AssetQOSDetail setQoS(Double qoS) {
		this.QoS = qoS;
		return this;
	}

	public String getLocationGroupDescription() {
		return locationGroupDescription;
	}

	public AssetQOSDetail setLocationGroupDescription(String locationGroupDescription) {
		this.locationGroupDescription = locationGroupDescription;
		return this;
	}

	public String getLocationDescription() {
		return locationDescription;
	}

	public AssetQOSDetail setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
		return this;
	}

	public String getLocationPrestigeName() {
		return locationPrestigeName;
	}

	public AssetQOSDetail setLocationPrestigeName(String locationPrestigeName) {
		this.locationPrestigeName = locationPrestigeName;
		return this;
	}

	public String getLocationTimezone() {
		return locationTimezone;
	}

	public AssetQOSDetail setLocationTimezone(String locationTimezone) {
		this.locationTimezone = locationTimezone;
		return this;
	}

	public Long getLocationPhoneNumber() {
		return locationPhoneNumber;
	}

	public AssetQOSDetail setLocationPhoneNumber(Long locationPhoneNumber) {
		this.locationPhoneNumber = locationPhoneNumber;
		return this;
	}

	public Integer getSummaryMonth() {
		return summaryMonth;
	}

	public AssetQOSDetail setSummaryMonth(Integer summaryMonth) {
		this.summaryMonth = summaryMonth;
		return this;
	}
	
	public String getAssetName() {
		return assetName;
	}

	public AssetQOSDetail setAssetName(String assetName) {
		this.assetName = assetName;
		return this;
	}

	public Integer getAssetID() {
		return assetID;
	}

	public AssetQOSDetail setAssetID(Integer assetID) {
		this.assetID = assetID;
		return this;
	}

	public String getModelName() {
		return modelName;
	}

	public AssetQOSDetail setModelName(String modelName) {
		this.modelName = modelName;
		return this;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public AssetQOSDetail setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
		return this;
	}

	public Integer getAssetTypeId() {
		return assetTypeId;
	}

	public AssetQOSDetail setAssetTypeId(Integer assetTypeId) {
		this.assetTypeId = assetTypeId;
		return this;
	}

	@Override
	public String toString() {
		String locQOS = "uptime is: " + upTime + " downtime is : " + downTime + " Year is : " + summaryYear + 
						" QOS is  : " + QoS;
		return locQOS;
	}
}
