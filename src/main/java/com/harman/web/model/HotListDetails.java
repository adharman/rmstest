package com.harman.web.model;

import java.sql.Blob;
import java.util.Date;
import java.util.Map;

import net.sourceforge.jtds.jdbc.BlobImpl;

public class HotListDetails {
	
	private String date;
	
	private String location;
	
	private String locationGroup;
	
	private String assetName;
	
	private String summary;
	
	private String assetStatus;
	
	private String statusImageLocation;
	
	private Integer locationID;
	
	private Integer assetID;
	
	private Integer parameterId;
	
	private String imageName;
	
	private Map<String, Object> assetDetails;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocationGroup() {
		return locationGroup;
	}

	public void setLocationGroup(String locationGroup) {
		this.locationGroup = locationGroup;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getAssetStatus() {
		return assetStatus;
	}

	public void setAssetStatus(String assetStatus) {
		this.assetStatus = assetStatus;
	}

	public String getStatusImageLocation() {
		return statusImageLocation;
	}

	public void setStatusImageLocation(String statusImageLocation) {
		this.statusImageLocation = statusImageLocation;
	}

	public Integer getLocationID() {
		return locationID;
	}

	public void setLocationID(Integer locationID) {
		this.locationID = locationID;
	}

	public Integer getAssetID() {
		return assetID;
	}

	public void setAssetID(Integer assetID) {
		this.assetID = assetID;
	}

	public Integer getParameterId() {
		return parameterId;
	}

	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}

	public Map<String, Object> getAssetDetails() {
		return assetDetails;
	}

	public void setAssetDetails(Map<String, Object> assetDetails) {
		this.assetDetails = assetDetails;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	
	
}
