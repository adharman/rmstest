package com.harman.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="[RMS].[dbo].[Location]")
public class Location {

	@Id
	@Column(name="Location_ID")
	private Integer locationId;
	@Column(name="Location_Group_ID")
	private Integer locationGroupId;
	@Column(name="Row_Version")
	private Integer rowVersion;
	@Column(name="Name")
	private String locationName;
	@Column(name="Description")
	private String desc;
	@Column(name="Phone_Number")
	private Double phoneNumber;
	@Column(name="Occupancy")
	private Integer occupancy;
	@Column(name="Map_URL")
	private String mapURL;
	@Column(name="Web_Control_URL")
	private String webControlURL;
	@Column(name="Timezone")
	private String timeZone;
	@Column(name="Owner")
	private String owner;
	@Column(name="Locale")
	private String locale;
	@Column(name="Maintenance_Mode_Enabled")
	private boolean maintenance;
	@Column(name="Prestige_ID")
	private Integer prestigeID;
	@Column(name="License_Information")
	private Integer licenseInforation;
	public Integer getLocationId() {
		return locationId;
	}
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}
	public Integer getLocationGroupId() {
		return locationGroupId;
	}
	public void setLocationGroupId(Integer locationGroupId) {
		this.locationGroupId = locationGroupId;
	}
	public Integer getRowVersion() {
		return rowVersion;
	}
	public void setRowVersion(Integer rowVersion) {
		this.rowVersion = rowVersion;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Double getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Double phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getOccupancy() {
		return occupancy;
	}
	public void setOccupancy(Integer occupancy) {
		this.occupancy = occupancy;
	}
	public String getMapURL() {
		return mapURL;
	}
	public void setMapURL(String mapURL) {
		this.mapURL = mapURL;
	}
	public String getWebControlURL() {
		return webControlURL;
	}
	public void setWebControlURL(String webControlURL) {
		this.webControlURL = webControlURL;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public boolean isMaintenance() {
		return maintenance;
	}
	public void setMaintenance(boolean maintenance) {
		this.maintenance = maintenance;
	}
	public Integer getPrestigeID() {
		return prestigeID;
	}
	public void setPrestigeID(Integer prestigeID) {
		this.prestigeID = prestigeID;
	}
	public Integer getLicenseInforation() {
		return licenseInforation;
	}
	public void setLicenseInforation(Integer licenseInforation) {
		this.licenseInforation = licenseInforation;
	}
	
	
}
