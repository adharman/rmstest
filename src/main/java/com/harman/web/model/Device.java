package com.harman.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Device {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int deviceid;
	
	public int getDeviceid() {
		return deviceid;
	}
	

	public void setDeviceid(int deviceid) {
		this.deviceid = deviceid;
	}
	

	@Column(name="usedhour")
	private int usedHour;
	@Column(name="capacity")
	private int capacity;
	@Column(name="modelno")
	private String modelNo;
	@Column(name="devicetype")
	private String devicetype;
	
	public int getUsedHour() {
		return usedHour;
	}
	
	public void setUsedHour(int usedHour) {
		this.usedHour = usedHour;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public String getModelNo() {
		return modelNo;
	}
	
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	public String getDevicetype() {
		return devicetype;
	}

	public void setDevicetype(String devicetype) {
		this.devicetype = devicetype;
	}

	
	
}
