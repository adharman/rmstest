package com.harman.web.utilities;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;

@Component
public class PasswordUtility implements PasswordEncoder {
	
	private String realmName = "RMS Security Realm";

	@Override
	public String encodePassword(String rawPass, Object salt) {
		String saltedPass = mergePasswordAndSalt(rawPass, (String)salt);
		System.out.println("Salt that got back is : " + saltedPass);
        MessageDigest messageDigest = getMessageDigest("MD5");
        
        String result = "";
        
        try
        {
            byte[] digest = messageDigest.digest(saltedPass.getBytes("UTF-8"));
            result = new String(Hex.encode(digest)); 
        }
        catch (UnsupportedEncodingException e)
        {
            throw new IllegalStateException("UTF-8 not supported!");
        }

        return result;
	}

	protected final MessageDigest getMessageDigest(String algorithm)
            throws IllegalArgumentException
    {
        try
        {
            return MessageDigest.getInstance(algorithm);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new IllegalArgumentException("No such algorithm [" + algorithm + "]");
        }
    }
	
	@Override
	public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
		String pass1 = encPass;  // ensure that encPass doesn't lead to null ptr ex
        String pass2 = encodePassword(rawPass, salt);

        return pass1.equals(pass2);
	}
	
	 protected String mergePasswordAndSalt(String password, String salt)
	    {
	        if (password == null)
	        {
	            password = "";
	        }
	        
	        if (realmName == null)
	        {
	            realmName = "";
	        }

	        if (salt == null)
	        {
	            salt = "";
	        }
	        
	        return salt + ":" + realmName + ":" + password;
	    }
	    

}
