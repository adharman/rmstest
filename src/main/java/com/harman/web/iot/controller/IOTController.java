package com.harman.web.iot.controller;

import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.harman.web.service.IOTService;

@RestController
public class IOTController {

	@Autowired
	private IOTService iotService;
	@RequestMapping(value = "/getconfstatus", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONObject getConfRoomStatus() throws SQLException, UnknownHostException {
		JSONObject json = new JSONObject();
		System.out.println("Inside getConfStatus Controller");
		org.json.JSONObject roomStatus = iotService.getCurrentConfStatus();
		if(((String)roomStatus.get("data")).equalsIgnoreCase("available")) {
			json.put("Status", true);
			json.put("Date/Time", roomStatus.get("published_at"));
			json.put("RoomName", "Vista");
		} else {
			json.put("Status", false);
			json.put("Date/Time", roomStatus.get("published_at"));
			json.put("RoomName", "Vista");
		}
		return json;
	}
	
	@RequestMapping(value = "/getavgmeetingtime", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONObject getAvgMeetingTimeInMinutes() throws SQLException, UnknownHostException {
		JSONObject json = new JSONObject();
		System.out.println("Inside getavgmeetingtime Controller");
		float avgMeetingTimeInMinutes = iotService.getAvgMeetngTimeInMinutes();
		json.put("AvgTimeInMinutes", avgMeetingTimeInMinutes);
		return json;
	}
	
	@RequestMapping(value = "/getCalendarHeatMap", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Map<String, Object>> getCalendarHeatMap() throws SQLException, UnknownHostException {
		System.out.println("Inside getcalendarheatmap Controller");
		List<Map<String, Object>> allIndividualDetailsList = iotService.getCalendarHeatMapData();
		//JSONObject json = new JSONObject();
		return allIndividualDetailsList;
	}
}
