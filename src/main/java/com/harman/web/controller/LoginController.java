package com.harman.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.harman.web.service.CommonService;

@RestController
public class LoginController {
	
	@Autowired
	
	private CommonService commonService;
	
	@RequestMapping(value="/userLogin",method=RequestMethod.POST,produces="application/json")
	@ResponseBody
	public JSONObject rmsUserlogin(@RequestBody JSONObject userJSON)
	{
		String username = (String) userJSON.get("username");
		String password = (String) userJSON.get("password");
		System.out.println("username is : " + username);
		System.out.println("password is " + password);
		JSONObject resultJSON = commonService.doUserLogin(username, password);
		return resultJSON;
	}
	
	@RequestMapping(value="/syncUserLogins",method=RequestMethod.POST,produces="application/json")
	@ResponseBody
	public Map<String, Integer> syncUserLogins(@RequestBody Map<String, String> userLoginMap)
	{
		Map<String, Integer> statusMap = new HashMap<String, Integer>();
		System.out.println("insde sync userlogins array");
		int status = commonService.syncUserLogins(userLoginMap);
		statusMap.put("status", status);
		return statusMap;
	}

	@RequestMapping(value="/getexistinglogins",method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public List<Map<String, Object>> getExistingLogins()
	{
		
		System.out.println("insde getexisting  userlogins flow");
		List<Map<String, Object>> users = commonService.getExistingLogins();
		return users;
	}

}
