package com.harman.web.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.harman.web.model.AssetQOSDetail;
import com.harman.web.model.Device;
import com.harman.web.model.HotListDetails;
import com.harman.web.model.LocationQOSDetail;
import com.harman.web.service.CommonService;

@RestController
public class AssetController {
	
	@Autowired
	private CommonService commonService;
	
	private static final Logger logger = LoggerFactory.getLogger(AssetController.class);
	
	//@RequestMapping(value="/findDevices",method=RequestMethod.GET,produces="application/json",headers="Accept=text/xml, application/json")
	@RequestMapping("/findDevices")	
	public  List<Device> findDevices()
	//public List<Device> findDevices()
	{
		/*// create a new Gson instance
		 Gson gson = new Gson();
		 // convert your list to json
		 List jsonCartList = gson.toJson(commonService.findAllDevices());
		 // print your generated json
		 System.out.println("jsonCartList: " + jsonCartList);
		//return commonService.findAllDevices();
		 */
		logger.info("-----------------------method find devices----------------------");
		 List<Device> devicelist=commonService.findAllDevices();
		 logger.info("--value----"+devicelist.toString());
		 return devicelist;
	}

	@RequestMapping("/findProjector/{id}")
	public Device findProjector(@PathVariable("id") int deviceId)
	{
		System.out.println("path variable--------"+deviceId);
		return commonService.findProjector(deviceId);
	}

	@RequestMapping("/finddevicebyName/{name}")
	public Device findDeviceByName(@PathVariable("name") String devicetype)
	{
		System.out.println("path variable--------"+devicetype);
		return commonService.findProjectorbyName(devicetype);
	}
	
	@RequestMapping(value = "/getQOSByLocation/{locationname}", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONObject getQOSByLocationAndItsAssests(@PathVariable("locationname") String locationName) {
		System.out.println("Inside getQOSByAsset controller");
		List<LocationQOSDetail> locationQOC = commonService.getQOSByLocation(locationName);
		List<AssetQOSDetail> assetQOC = commonService.getAssetQOCByLocation(locationName);
		JSONObject json = new JSONObject();
		json.put("LocationQOC", locationQOC);
		json.put("AssetsQOC", assetQOC);
		return json;
	}
	
	@RequestMapping(value = "/getProjectorStatus", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Map<String, String>> getProjectorStatus() {
		System.out.println("Inside getProjectorStatus controller");
		List<Map<String, String>> projectorStatusArray = commonService.getProjectorStatus();
		return projectorStatusArray;
	}
	
	@RequestMapping(value = "/hotlistdetails/{locationname}", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONObject getDashBoardHotListDetails(@PathVariable("locationname") String locationName) throws SQLException {
		System.out.println("Inside hotlist details controller");
		JSONObject hotlistDetailsAndAssetDetails = commonService.getHostListDetails(locationName);
		return hotlistDetailsAndAssetDetails;
	}
	
	@RequestMapping(value = "/AssetPowerConsumptionByLocation/{locationname}", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JSONObject getAssetsPowerConsumptionByLocation(@PathVariable("locationname") String locationName) throws SQLException {
		JSONObject hotlistDetailsAndAssetDetails = commonService.getAssetsPowerConsumptionByLocation(locationName);
		return hotlistDetailsAndAssetDetails;
	}
	

}
