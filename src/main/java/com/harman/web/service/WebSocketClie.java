package com.harman.web.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.simple.JSONObject;

public class WebSocketClie extends WebSocketClient {

	public WebSocketClie(URI serverURI) {
		super(serverURI);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onOpen( ServerHandshake handshakedata ) {
		System.out.println( "Connected" );

	}

	@Override
	public void onMessage( String message ) {
		System.out.println( "got: " + message );

	}

	@Override
	public void onClose( int code, String reason, boolean remote ) {
		System.out.println( "Disconnected" );
		System.exit( 0 );

	}

	@Override
	public void onError( Exception ex ) {
		ex.printStackTrace();

	}
	
	public void sendNotification(JSONObject onlineAssets) throws URISyntaxException {
		WebSocketClie chatclient = new WebSocketClie(new URI("ws://localhost:1337"));
		chatclient.send(onlineAssets.toJSONString());
		chatclient.close();

		}

	}