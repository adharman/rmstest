package com.harman.web.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.harman.web.dao.CommonDAO;
import com.harman.web.model.AssetQOSDetail;
import com.harman.web.model.Device;
import com.harman.web.model.HotListDetails;
import com.harman.web.model.LocationQOSDetail;
import com.harman.web.model.UserDetail;

@Service
@Transactional
public class CommonServiceImpl implements CommonService {
	
	@Autowired
	private CommonDAO commonDAO; 

	@Override
	public void saveDevice(Device device) {
		// TODO Auto-generated method stub
		commonDAO.saveDevice(device);
		
	}

	@Override
	public List<Device> findAllDevices() {
		// TODO Auto-generated method stub
		return commonDAO.findAllDevices();
	}

	@Override
	public Device findProjector(int deviceId) {
		// TODO Auto-generated method stub
		return commonDAO.findProjector(deviceId);
	}
	

	@Override
	public Device findProjectorbyName(String devicetype) {
		// TODO Auto-generated method stub
		return commonDAO.findProjectorbyName(devicetype);
	}

	@Override
	public UserDetail rmsUserlogin(UserDetail user) {
		System.out.println("in Service layer");
		return commonDAO.rmsUserlogin(user);
	}

	@Override
	public List<LocationQOSDetail> getQOSByLocation(String locationName) {
		return commonDAO.getQOSByLocation(locationName);
	}

	@Override
	public List<AssetQOSDetail> getAssetQOCByLocation(String locationName) {
		// TODO Auto-generated method stub
		return commonDAO.getAssetsQOCByLocation(locationName);
	}

	@Override
	public JSONObject getHostListDetails(String locationName) throws SQLException {
		// TODO Auto-generated method stub
		return commonDAO.getHotListDetailsByLocation(locationName);
	}

	@Override
	public JSONObject getAssetsPowerConsumptionByLocation(String locationName) {
		// TODO Auto-generated method stub
		return commonDAO.getAssetsPowerConsumptionByLocation(locationName);
	}

	@Override
	public JSONObject doUserLogin(String username, String password) {
		// TODO Auto-generated method stub
		return commonDAO.doUserLogin(username, password);
		
	}

	@Override
	public List<Map<String, String>> getProjectorStatus() {
		// TODO Auto-generated method stub
		return commonDAO.getProjectorStatus();
	}

	@Override
	public int syncUserLogins(Map<String, String> userLogin) {
		// TODO Auto-generated method stub
		return commonDAO.syncUserLogins(userLogin);
	}

	@Override
	public List<Map<String, Object>>  getExistingLogins() {
		// TODO Auto-generated method stub
		return commonDAO.getExistinglogins();
	}
}
