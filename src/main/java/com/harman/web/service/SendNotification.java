package com.harman.web.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.simple.JSONObject;

import com.harman.web.controller.AssetController;
import com.harman.web.iot.controller.IOTController;

public class SendNotification extends TimerTask{
	
	private IOTController iotController = new IOTController();
	private AssetController assetController = new AssetController();
	@Override
	public void run() {
		System.out.println("Hello World!"); 
//		Socket webSocket = new Sock
		try {
			List<Map<String, String>> responseJSONtOBeSentToWebSocket = new ArrayList<>();
			//JSONObject json = iotController.getConfRoomStatus();
			//String roomStatus = (String) json.get("Status");
			String roomStatus = "Room_Available";
			if(roomStatus.equalsIgnoreCase("Room_Available")) {
				//Room is not being used by anyone, check if any configured devices are online , if any devices are online then send a 
				// a notification to the registered clients sayig room is not in use and can switch off the devices are turn them to standBy
				
				//Firstly get the all devices status, if any device status are online then only send a notification
				JSONObject jsonresponse = assetController.getDashBoardHotListDetails("NX1200");
				List<Map<String, Object>> hotListDetails = (List<Map<String, Object>>)jsonresponse.get("HotListDetails");
				for(Map<String, Object> hotListDetail : hotListDetails) {
					String hotListItemStatus = (String) hotListDetail.get("assetStatus");
					if(!hotListItemStatus.equalsIgnoreCase("Online")) {
						Map<String,String> hotListDetailsToBeSent = new HashMap<>();
						hotListDetailsToBeSent.put("Room", "NX1200");
						hotListDetailsToBeSent.put("AssetName", (String) hotListDetail.get("assetName"));
						hotListDetailsToBeSent.put("CurrentStatus", "Online");
						hotListDetailsToBeSent.put("CurrentStatusOfTheRoom", "Not_In_Use");
						responseJSONtOBeSentToWebSocket.add(hotListDetailsToBeSent);
					}
				}
			}
			
			if(!responseJSONtOBeSentToWebSocket.isEmpty()) {
				for(Map<String,String> onlineAssets : responseJSONtOBeSentToWebSocket) {
					JSONObject jsonresp = new JSONObject();
					jsonresp.put("Room", "NX1200");
					jsonresp.put("CurrentStatus", (String) onlineAssets.get("CurrentStatus"));
					jsonresp.put("AssetName", (String) onlineAssets.get("assetName"));
					jsonresp.put("CurrentStatusOfTheRoom", "Not_In_Use");
					WebSocketClie webSocCli = new WebSocketClie(new URI( "ws://localhost:1337"));
					webSocCli.sendNotification(jsonresp);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	public static void main(String args[]) {
		Timer timer = new Timer();
		timer.schedule(new SendNotification(), 0, 10000);
		
	}
}
