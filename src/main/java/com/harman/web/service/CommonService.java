package com.harman.web.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.harman.web.model.AssetQOSDetail;
import com.harman.web.model.Device;
import com.harman.web.model.HotListDetails;
import com.harman.web.model.LocationQOSDetail;
import com.harman.web.model.UserDetail;

public interface CommonService {
	
void saveDevice(Device device);
    
    List<Device> findAllDevices();

	Device findProjector(int deviceId);

	Device findProjectorbyName(String devicetype);

	UserDetail rmsUserlogin(UserDetail user);

	List<LocationQOSDetail> getQOSByLocation(String locationName);

	List<AssetQOSDetail> getAssetQOCByLocation(String locationName);

	JSONObject getHostListDetails(String locationName) throws SQLException;

	JSONObject getAssetsPowerConsumptionByLocation(String locationName);

	JSONObject doUserLogin(String username, String password);

	List<Map<String, String>> getProjectorStatus();

	int syncUserLogins(Map<String, String> userLoginMap);

	List<Map<String, Object>> getExistingLogins();


}
