package com.harman.web.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.io.ByteBufferPool.Bucket;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class IOTService {

	public org.json.JSONObject getCurrentConfStatus() throws UnknownHostException {
		// TODO Auto-generated method stub
		SearchResponse response  = createTransportClient().prepareSearch().setIndices("motionsensor").setTypes("data")
		.setQuery(QueryBuilders.matchAllQuery())
		.addSort("published_at", SortOrder.DESC).setSize(1).execute().actionGet();
		SearchHits hits = response.getHits();
		//hits.
		System.out.println(hits.getHits());
		SearchHit[] searchHits = hits.getHits();
		
			String source = searchHits[0].getSourceAsString();
			org.json.JSONObject json = new org.json.JSONObject(source);
			System.out.println(source);
		
		return json;
	}

	public Client createTransportClient() {
		  Settings settings = Settings.builder()
			        .put("cluster.name", "code-buster").build();
		  TransportClient client = null;
		try {
			client = new PreBuiltTransportClient(settings)
				        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		    return client;
		}

	public float getAvgMeetngTimeInMinutes() {
		int count = 0;
		int sumOfTimeChunks=0;
		SearchResponse scrollResp  = createTransportClient().prepareSearch().setIndices("motionsensor").setTypes("avgtimechunks")
				.setScroll(new TimeValue(60000))
				.setQuery(QueryBuilders.matchAllQuery())
				.setSize(100)
				.execute().actionGet();
		
		do {
		    for (SearchHit hit : scrollResp.getHits().getHits()) {
		        //Handle the hit...
		    	count ++;
				String source = hit.getSourceAsString();
				org.json.JSONObject json = new org.json.JSONObject(source);
				sumOfTimeChunks += (Integer)json.getInt("timechunk");
				System.out.println(source);
		    }
		    scrollResp = createTransportClient().prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(60000)).execute().actionGet();
		} while(scrollResp.getHits().getHits().length != 0); 
				
				//figure out avg meeting time in hours
				float avgMeetingTimeInMinutesInFloat = sumOfTimeChunks/count;
				Integer avgMeetingTimeInMinutes = (int) avgMeetingTimeInMinutesInFloat;
				//float avgMeetingTimeInHours = avgMeetingTimeInMinutes/60;
				return avgMeetingTimeInMinutes;
	}

	public List<Map<String, Object>> getCalendarHeatMapData() {
		List<Map<String, Object>> allIndividualDateDetails = new ArrayList<Map<String, Object>>();
		System.out.println("Calendar Day Wednesday ,calendarTime data for the month of march");
		//load json file create a JSON Object out of it.
		org.json.JSONObject modifiedJSONTemplate = null;
		        
        //process calendar time data for each calendar day e.g. for Mon,Tue process calendarTime data
        
		List<String> distinctDates = getDistinctDates();
		for(String distinctDate : distinctDates) {
			Map<String, Object> individualDateDetails = new HashMap<String, Object>();
		    //individualDateDetails.put("rangeStartDate", "Sat Apr 08 2017 23:59:59 GMT+0530");
			//individualDateDetails.put("rangeEndDate", "Mon Apr 10 2017 23:59:59 GMT+0530");
			individualDateDetails.put("date", distinctDate);
			//get distinct date documents 
			Map<String,Object> perTimeDetailsMap = getDistinctDateDocs(distinctDate);
			individualDateDetails.put("total", (Long) perTimeDetailsMap.get("TotaLSecs"));
			individualDateDetails.put("details", (List<Map<String, Object>>) perTimeDetailsMap.get("DistinctDateDocs"));
			individualDateDetails.put("summary", (List<Map<String, Object>>) perTimeDetailsMap.get("DistinctDateDocs"));
			allIndividualDateDetails.add(individualDateDetails);
		}
		return allIndividualDateDetails;
	}

	private Map<String,Object> getDistinctDateDocs(String distinctDate) {
		// TODO Auto-generated method stub
		Long secsTotalValue=(long) 0;
		Map<String,Object> finalMap = new HashMap<String,Object>();
		List<Map<String, Object>> perTimeDetailsList = new ArrayList<Map<String, Object>>();
		SearchResponse scrollResp  = createTransportClient().prepareSearch().setIndices("motionsensor").setTypes("calendarheatmap")
				.setScroll(new TimeValue(60000))
				.setQuery(QueryBuilders.matchQuery("date", distinctDate))
				.setSize(100)
				.execute().actionGet();
		
		 for (SearchHit hit : scrollResp.getHits().getHits()) {
		        //Handle the hit...
		    	Map<String, Object> perTimeDetailObject = new HashMap<String,Object>();
		    	String source = hit.getSourceAsString();
				org.json.JSONObject json = new org.json.JSONObject(source);
		    	perTimeDetailObject.put("name", "Viper");
		    	String[] publishedDate = json.getString("published_date").split("T");
		    	String yearAndDay = publishedDate[0];
		    	//String hours[] = publishedDate[1].split(".");
		    	String formattedPublishedDate = yearAndDay + " " + publishedDate[1];
		    	perTimeDetailObject.put("date", formattedPublishedDate);
		    	int seconds = json.getInt("TotalConsumedTimeInMinutes")*60;
		    	secsTotalValue += seconds;
		    	perTimeDetailObject.put("value", seconds);
		    	perTimeDetailsList.add(perTimeDetailObject);
		    }
		 
		 	finalMap.put("DistinctDateDocs", perTimeDetailsList);
		 	finalMap.put("TotaLSecs", secsTotalValue);
		 	return finalMap;
	}

	private List<String> getDistinctDates() {
		List<String> distinctTerms = new ArrayList<String>();
		try {
			TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders.terms("dates").field("date");
			
			SearchResponse scrollResp  = createTransportClient().prepareSearch().setIndices("motionsensor").setTypes("calendarheatmap").addAggregation(termsAggregationBuilder).setSize(0).execute().actionGet();
			Terms agg1 = scrollResp.getAggregations().get("dates");
			List<org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket> buckets = agg1.getBuckets();
			for(org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket bucket : buckets) {
				String keyBucket = bucket.getKeyAsString();
				distinctTerms.add(keyBucket.split("T")[0]);
				System.out.println("Bucket key is : " + keyBucket.split("T")[0]);
			}
					} catch (Exception e) {
			e.printStackTrace();
		}
		return distinctTerms;
	}
}