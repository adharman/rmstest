package com.harman.web.configuration;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.harman.web.model.Device;
import com.harman.web.service.CommonService;

public class TestOperation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("rmsent-context.xml");
		
		CommonService service=(CommonService)ctx.getBean("CommonService");
		List<Device> devices = service.findAllDevices();
        for (Device device : devices) {
            System.out.println(device);
        }
	}

}
