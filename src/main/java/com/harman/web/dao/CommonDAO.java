package com.harman.web.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.harman.web.model.AssetQOSDetail;
import com.harman.web.model.Device;
import com.harman.web.model.HotListDetails;
import com.harman.web.model.LocationQOSDetail;
import com.harman.web.model.UserDetail;

public interface CommonDAO {
	void saveDevice(Device device);
    
    List<Device> findAllDevices();

	Device findProjector(int deviceId);

	Device findProjectorbyName(String deviceType);

	
	UserDetail rmsUserlogin(UserDetail user);

	List<LocationQOSDetail> getQOSByLocation(String locationName);

	List<AssetQOSDetail> getAssetsQOCByLocation(String locationName);

	JSONObject getHotListDetailsByLocation(String locationName) throws SQLException;

	JSONObject getAssetsPowerConsumptionByLocation(String locationName);

	JSONObject doUserLogin(String username, String password);

	List<Map<String, String>> getProjectorStatus();

	int syncUserLogins(Map<String, String> userLogin);

	List<Map<String, Object>> getExistinglogins();

}
