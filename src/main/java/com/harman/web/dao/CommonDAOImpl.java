package com.harman.web.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.harman.web.configuration.HibernateConfiguration;
import com.harman.web.model.AssetQOSDetail;
import com.harman.web.model.Device;
import com.harman.web.model.HotListDetails;
import com.harman.web.model.LocationQOSDetail;
import com.harman.web.model.UserDetail;
import com.harman.web.utilities.PasswordUtility;

@Repository
public class CommonDAOImpl  extends HibernateConfiguration implements CommonDAO{

	private static final Logger logger = LoggerFactory.getLogger(CommonDAOImpl.class);
	@Autowired
	private PasswordUtility passwordUtility;


	@Override
	public UserDetail rmsUserlogin(UserDetail user) {
		logger.info("--frm dao layer ---input value--"+user.getUsername());
		Criteria criteria = getSession().createCriteria(UserDetail.class);
        criteria.add(Restrictions.eq("username",user.getUsername()));
        logger.info("------by name----"+(UserDetail) criteria.uniqueResult());
        return (UserDetail) criteria.uniqueResult();
	}

	@Override
	public List<LocationQOSDetail> getQOSByLocation(String locationName) {		
				String queryString = "EXEC [RMS].dbo.sp_QOSByLocation @Username= admin,@LocationName = " + locationName;
				Query query = getSession().createSQLQuery(queryString);
				List<Object[]> rows = query.list();
				List<LocationQOSDetail> locationQOCList = 
						rows.parallelStream()
							.filter((row) -> (Integer.parseInt(row[3].toString()) == 2017))
							.map(row -> new LocationQOSDetail().setLocationGroup(row[0].toString())
														.setLocationName(row[1].toString())
														.setLocationID(Integer.parseInt(row[2].toString()))
														.setSummaryYear(Integer.parseInt(row[3].toString()))
														.setSummaryMonth(null!= row[4] ? Integer.parseInt(row[4].toString()) : null)
														.setSummaryDay(null != row[5] ? Integer.parseInt(row[5].toString()) : null)
														.setDownTime(Long.parseLong(row[6].toString()))
														.setUpTime(Long.parseLong(row[7].toString()))
														.setQoS(Double.parseDouble(row[8].toString()))
														.setLocationGroupDescription(row[9].toString())
														.setLocationDescription(null != row[10] ? row[10].toString() : null)
														.setLocationPrestigeName(row[11].toString())
														.setLocationTimezone(null != row[12] ? row[12].toString() : null)
														.setLocationPhoneNumber(null != row[13] ? Long.parseLong(row[13].toString()) : null))
							.collect(Collectors.toList());
				Collections.sort(locationQOCList,(locqos1,locqos2) -> locqos2.getSummaryYear().compareTo(locqos1.getSummaryYear()));
				return locationQOCList;
	}

	@Override
	public List<AssetQOSDetail> getAssetsQOCByLocation(String locationName) {
		String queryString = "EXEC [RMS].dbo.sp_QOSByAsset @Username = admin, @LocationName = "+ locationName;
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		return  rows.parallelStream()
			.filter((row) -> (Integer.parseInt(row[5].toString()) ==  2017))
			.map(row  -> new AssetQOSDetail().setLocationGroup(row[0].toString())
											 .setLocationName(row[1].toString())
											 .setLocationID(Integer.parseInt(row[2].toString()))
											 .setAssetName(row[3].toString())
											 .setAssetID(Integer.parseInt(row[4].toString()))
											 .setSummaryYear(Integer.parseInt(row[5].toString()))
											 .setSummaryMonth(null!= row[6] ? Integer.parseInt(row[6].toString()) : null)
											 .setSummaryDay(null != row[7] ? Integer.parseInt(row[7].toString()) : null)
											 .setDownTime(Long.parseLong(row[8].toString()))
											 .setUpTime(Long.parseLong(row[9].toString()))
											 .setQoS(Double.parseDouble(row[10].toString()))
											 .setLocationGroupDescription(row[11].toString())
											 .setLocationDescription(null != row[12] ? row[12].toString() : null)
											 .setLocationPrestigeName(row[13].toString())
											 .setLocationTimezone(null != row[14] ? row[14].toString() : null)
											 .setLocationPhoneNumber(null != row[15] ? Long.parseLong(row[15].toString()) : null)
											 .setModelName(null != row[16] ? row[16].toString() : null)
											 .setManufacturerName(null != row[17] ? row[17].toString() : null)
											 .setAssetTypeId(null != row[18] ? Integer.parseInt(row[18].toString()) : null))
			.collect(Collectors.toList());
	}

	@Override
	public JSONObject getHotListDetailsByLocation(String locationName) throws SQLException {
		JSONObject hotlistAndOhterAssetDetails = new JSONObject();
		List<HotListDetails> hotListDetailsList = new ArrayList<HotListDetails>();
		List assetDetailsList = new ArrayList();
		String queryString = " select hl.[Created],loc.Name as location, "+
				" locgrp.Name as location_group,assest.Name as asset_name,ap.Name,ap.Value,ap.Units, "+
				" st.Name as status,img.Image as status_image,img.Name as image_name, "+ "loc.Location_ID as location_id,assest.Asset_ID as asset_id,hl.Parameter_ID" +
				" from [RMS].[dbo].[Hotlist] hl,[RMS].[dbo].[Hotlist_Status_Type] hst,[RMS].[dbo].[Status_Type] st, "+
				" [RMS].[dbo].[Asset_Parameter] ap,[RMS].[dbo].[Asset] assest, [RMS].[dbo].[Location] loc,"+
				" [RMS].[dbo].[Location_Group] locgrp,[RMS].[dbo].[Image] img "+
				" where  hl.Hotlist_Record_ID = hst.Hotlist_Record_ID and "+
				" hl.Hotlist_Record_ID = hst.Hotlist_Record_ID and "+
				" hst.Status_Type_ID = st.Status_Type_ID  and "+
				" hl.Parameter_ID = ap.Parameter_ID and "+
				" ap.Asset_ID = assest.Asset_ID and "+
				" assest.Location_ID = loc.Location_ID and "+
				" loc.Location_Group_ID = locgrp.Location_Group_ID and "+
				" hl.Parameter_ID = ap.Parameter_ID and "+
				"img.Image_ID = st.Image_ID and" +
				" loc.Name = '" +locationName + "'";
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			String endDelimeter = "'";
			String unitsIfAny = null != row[6] ? " " + row[6].toString() : "";
			if(!"".equals(unitsIfAny)) {
				endDelimeter = "'";
			}
			String summary = "Paramater " + "'" + row[4].toString() + "'" + " has exceeded its configured threshold.The current value is " + "'" + row[5]  + unitsIfAny + endDelimeter ;
			File image = new File("D:\\images\\"+row[9]);
		    try {
				FileOutputStream fos = new FileOutputStream(image);
				byte barr[] = ((Blob)row[8]).getBytes(1, (int)(((Blob)row[8]).length()));
				try {
					fos.write(barr);
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			} catch (FileNotFoundException e) {
				System.out.println("file not found");
			}
			String imageLoc = image.getPath();
			HotListDetails hotList = new HotListDetails();
			hotList.setDate(row[0].toString());
			hotList.setLocation(row[1].toString());
			hotList.setLocationGroup(row[2].toString());
			hotList.setAssetName(row[3].toString());
			hotList.setSummary(summary);
			hotList.setAssetStatus(row[7].toString());
			hotList.setStatusImageLocation(imageLoc);
			hotList.setImageName(image.getName());
			hotList.setLocationID(Integer.parseInt(row[10].toString()));
			hotList.setAssetID(Integer.parseInt(row[11].toString()));
			hotList.setParameterId(Integer.parseInt(row[12].toString()));
			//hotListDetailsList.add(hotList);
			
			//Process asset details
			JSONObject assetDetails = processAssetAttributes(hotList);
			//assetDetailsList.add(assetDetails);
			hotList.setAssetDetails(assetDetails);
			hotListDetailsList.add(hotList);
		}
		
		hotlistAndOhterAssetDetails.put("HotListDetails", hotListDetailsList);
		//hotlistAndOhterAssetDetails.put("AssetDetails", assetDetailsList);
		return hotlistAndOhterAssetDetails;
	}

	private JSONObject processAssetAttributes(HotListDetails hotListDetails) {
		JSONObject assetDetailsJSON = new JSONObject();
		JSONObject assetLocationDetails = processLocationDetails(hotListDetails.getAssetName(), hotListDetails.getLocation());
		JSONObject clientGatewayDetails = processClientGatewayDetails(hotListDetails.getAssetName(), hotListDetails.getLocation(),hotListDetails.getLocationID());
		JSONObject assetBasicDetails = processAssetBasicDetails(hotListDetails);
		JSONObject assetMetaInformation = processAssetMetaInformation(hotListDetails);
		JSONObject thresholdInformation = processThresholdInformation(hotListDetails);
		JSONObject assetParameterDetails = processAssetParameterDetails(hotListDetails);
		assetDetailsJSON.put("AssetName", hotListDetails.getAssetName());
		assetDetailsJSON.put("Details",assetBasicDetails);
		assetDetailsJSON.put("Location", assetLocationDetails);
		assetDetailsJSON.put("Asset",assetMetaInformation);
		assetDetailsJSON.put("Parameter",assetParameterDetails);
		assetDetailsJSON.put("Threshold",thresholdInformation);
		assetDetailsJSON.put("Client_Gateway", clientGatewayDetails);
		return assetDetailsJSON;
	}

	private JSONObject processAssetParameterDetails(HotListDetails hotListDetails) {
		JSONObject assetParameterInformation = new JSONObject();
		String queryString = "SELECT ap.Name as parameter_name,apt.Name as parameter_type,ap.Value as Curentvalue from [RMS].[dbo].[Asset_Parameter] ap,"
								+" [RMS].[dbo].[Asset_Parameter_Type] apt where ap.Asset_Parameter_Type_ID = apt.Asset_Parameter_Type_ID and"
								+" ap.Parameter_ID =" + hotListDetails.getParameterId();
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			assetParameterInformation.put("Name", row[0].toString());
			assetParameterInformation.put("Parameter_Type", row[1].toString());
			assetParameterInformation.put("Data_Type", "");
			assetParameterInformation.put("Current_Value",row[2].toString());
		}
		return assetParameterInformation;

	}

	private JSONObject processThresholdInformation(HotListDetails hotListDetails) {
		JSONObject thresholdInformationJSON = new JSONObject();
		String queryString = "select apt.[Name],status.Name as status_type,aptco.Name as threshold_comparator,apt.threshold FROM "
								+ " [RMS].[dbo].[Asset_Parameter_Threshold] apt,[RMS].[dbo].[Status_type] status,"
								+ " [RMS].[dbo].[Asset_Parameter_Threshold_Compare_Operator] aptco"
								+ " where apt.Status_Type_ID = status.Status_Type_ID and" 
								+ " apt.Threshold_Compare_Operator_ID = aptco.Threshold_Compare_Operator_ID"
								+ " and apt.Parameter_ID = " + hotListDetails.getParameterId();
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			thresholdInformationJSON.put("Name", row[0].toString());
			thresholdInformationJSON.put("Status_Type", row[1].toString());
			thresholdInformationJSON.put("Image_Location", hotListDetails.getStatusImageLocation());
			thresholdInformationJSON.put("Image_Name", hotListDetails.getImageName());
			thresholdInformationJSON.put("Compare_Operator", null != row[2] ? row[2].toString() : "");
			thresholdInformationJSON.put("Threshold", null != row[3] ? row[3].toString() : "");
		}
		return thresholdInformationJSON;
	}

	private JSONObject processAssetMetaInformation(HotListDetails hotListDetails) {
		JSONObject assetMetaInformationJSON = new JSONObject();
		String queryString = "SELECT [Asset_Name],[Asset_Type_Name],[Asset_Serial_Number],[Asset_Firmware_Version],[Manufacturer_Name],"
				+ "[Model_Name] FROM [RMS].[dbo].[Asset_View] where [Asset_ID] ="+hotListDetails.getAssetID();
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			assetMetaInformationJSON.put("Asset_Name", row[0].toString());
			assetMetaInformationJSON.put("Asset_Type", row[1].toString());
			assetMetaInformationJSON.put("Device_ID", "");
			assetMetaInformationJSON.put("Serial_Number", null != row[2] ? row[2].toString() : "");
			assetMetaInformationJSON.put("Firmware", null != row[3] ? row[3].toString() : "");
			assetMetaInformationJSON.put("Manufacturer", null != row[4] ? row[4].toString() : "");
			assetMetaInformationJSON.put("Model", null != row[5] ? row[5].toString() : "");
			assetMetaInformationJSON.put("Paramters", "");
			assetMetaInformationJSON.put("Log", "");
			assetMetaInformationJSON.put("Links", "");
		}
		return assetMetaInformationJSON;
	}

	private JSONObject processAssetBasicDetails(HotListDetails hotListDetails) {
		JSONObject basicDetailsJSON = new JSONObject();
		basicDetailsJSON.put("Time_Of_Occurrence", hotListDetails.getDate());
		basicDetailsJSON.put("Relative_Time", hotListDetails.getDate());
		basicDetailsJSON.put("Location", hotListDetails.getLocation());
		basicDetailsJSON.put("Description", hotListDetails.getSummary());
		return basicDetailsJSON;
	}

	private JSONObject processClientGatewayDetails(String assetName, String locationName, Integer locationID) {
		JSONObject clientGatewayJSON = new JSONObject();
		String queryString = "SELECT [Client_Gateway_Name],[Client_Gateway_UID],[Client_Gateway_IP_Address],[Client_Gateway_MAC_Address]"
	  +",[Client_Gateway_RMS_SDK_Version]"
      +",[Client_Gateway_Communication_Protocol_ID]"
      +",[Client_Gateway_Communication_Protocol_Version]"
      +",[Client_Gateway_Communication_Active]"
      +"FROM [RMS].[dbo].[Client_Gateway_View] where [Client_Gateway_Location_ID] = '" + locationID + "'";
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			clientGatewayJSON.put("Client_Name", row[0].toString());
			clientGatewayJSON.put("Client_UID", row[1].toString());
			clientGatewayJSON.put("IP_Address", row[2].toString());
			clientGatewayJSON.put("MAC_Address", null != row[3] ? row[3].toString() : "");
			clientGatewayJSON.put("RMS_SDK_Version", row[4].toString());
			clientGatewayJSON.put("RMS_SDK_Protocol", Integer.parseInt(row[5].toString()));
			clientGatewayJSON.put("RMS_SDK_Protocol", Integer.parseInt(row[6].toString()));
			clientGatewayJSON.put("Communication_Active", row[7]);
		}
		return clientGatewayJSON;
	}
	private JSONObject processLocationDetails(String assetName, String locationName) {
		JSONObject locationJSON = new JSONObject();
		String queryString = " SELECT locView.Location_Name,locView.Location_Group_Name,locView.Location_Prestige_Name,locView.Location_Timezone,locView.Location_Phone_Number,locView.Location_Web_Control_URL,locView.Location_Map_URL  " +
				"FROM [RMS].[dbo].[Location_View] locView where locView.Location_Name = '" + locationName + "'";
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			locationJSON.put("Name", row[0].toString());
			locationJSON.put("Group", row[1].toString());
			locationJSON.put("Classification", row[2].toString());
			locationJSON.put("Time_Zone", null != row[3] ? row[3].toString() : "");
			locationJSON.put("Telephone", null != row[4] ? row[4].toString() : "");
			locationJSON.put("Web_Control", row[5].toString());
			locationJSON.put("Map_URL", null != row[6] ? row[6].toString() : "");
			locationJSON.put("Assets", "");
			locationJSON.put("Links", "");
		}
		return locationJSON;
		}

	@Override
	public JSONObject getAssetsPowerConsumptionByLocation(String locationName) {
		JSONObject assetsPowerConsumptionJSON = new JSONObject();
		assetsPowerConsumptionJSON.put("Location", locationName);
		List<JSONObject> assetsPowerConsumptionList = new ArrayList<JSONObject>();
		String queryString = "select Name,Wattage_Usage from fn_GetAssetEnergyUsageByLocation('admin','0','5')";
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows = query.list();
		rows
			.parallelStream()
			.forEach((row) -> {
			JSONObject assetPowerConsumption = new JSONObject();
			assetPowerConsumption.put("Asset_Type", row[0].toString());
			assetPowerConsumption.put("Usage", Float.parseFloat(row[1].toString()));
			assetPowerConsumption.put("Summary_Year", "2017");
			assetPowerConsumption.put("Units", "watt");
			assetsPowerConsumptionList.add(assetPowerConsumption);
		});
		assetsPowerConsumptionJSON.put("assetsPowerConsumption", assetsPowerConsumptionList);
		return assetsPowerConsumptionJSON;
	}

	@Override
	public void saveDevice(Device device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Device> findAllDevices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Device findProjector(int deviceId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Device findProjectorbyName(String deviceType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject doUserLogin(String username, String rawPass) {
		// TODO Auto-generated method stub
		String firstname = "";
		String lastname = "";
		JSONObject json = new JSONObject();
		String queryString = "select [Username],[password],[First_Name],[Last_Name] from [RMS].[dbo].[User] where [Username] = '" + username + "'";
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows  = query.list();
		if(!rows.isEmpty()) {
			Object[] user = rows.get(0);
			String userName = user[0].toString();
			String encPass = user[1].toString();
			System.out.println("username from db is : " + userName);
			System.out.println("password from db is : " + encPass);
			System.out.println("_____________________(((((((((((((((((" + passwordUtility.isPasswordValid(encPass, rawPass, userName));
			if(passwordUtility.isPasswordValid(encPass, rawPass, userName)) {
				firstname = user[2].toString();
				lastname = user[3].toString();
				json.put("firstname", firstname);
				json.put("lastname", lastname);
				json.put("status", 1);
				} else {
					json.put("firstname", firstname);
					json.put("lastname", lastname);
					json.put("status", 0);
				}
		}else {
			json.put("firstname", firstname);
			json.put("lastname", lastname);
			json.put("status", 0);
		}
		return json;
	}

	@Override
	public List<Map<String, String>> getProjectorStatus() {
		List<Map<String, String>> jsonList = new ArrayList<Map<String,String>>();
		String queryString = "select Value from [RMS].[dbo].[Asset_Parameter] a  where a.[Key]='projector.lamp.power' and a.Asset_ID in (select Asset_ID from [RMS].[dbo].[Asset] where Location_ID=13 and Name='Video Projector')";
		Query query = getSession().createSQLQuery(queryString);
		List<Object[]> rows  = query.list();
		String actualValue = rows.toString().replaceAll("\\[","");
		System.out.println(actualValue);
		if(actualValue.equalsIgnoreCase("On]")) {
			Map<String, String> map = new HashMap<String,String>();
			map.put("text", "Device 'video Projector' current status is 'Turned On',Please Turn off to save power!");
			jsonList.add(map);
		}
		return jsonList;
	}

	@Override
	public int syncUserLogins(Map<String, String> userLogin) {
		// TODO Auto-generated method stub
		
			
					//passwordUtility.encodePassword((String) user.get("password"), (String) user.get("username"));
			int status = upsertUserLogin((String)userLogin.get("password"),userLogin);
		
		return status;
	}

	private int upsertUserLogin(String encodedPassword, Map<String, String> user) {
		// TODO Auto-generated method stub
		int status = 0;
		String userName = (String) user.get("username");
		String password = encodedPassword;
		String firstName = (String) user.get("firstname");
		String lastName = (String) user.get("lastname");
		
		String queryString = "insert into [RMS].[dbo].[User] values(1,1,:username,:password,:firstname,:lastname,1,0,0,0,1,NULL,0,NULL,0,NULL,0,1,0)";
		System.out.println("query is : " + queryString.toString());
		Query query = getSession().createSQLQuery(queryString.toString());
		query.setParameter("username", userName);
		query.setParameter("password", password);
		query.setParameter("firstname", firstName);
		query.setParameter("lastname", lastName);
		int rows = query.executeUpdate();
		if(rows == 1) {
			System.out.println("user : " + userName + " successfully inserted");
			assignRoleToTheGivenUser(userName);
			status =1;
		}
		return status;
	}

	private void assignRoleToTheGivenUser(String userName) {
		List rows = getUserIdGivenUserName(userName);
		Integer userId = Integer.parseInt(rows.get(0).toString());
		String createRoleQueryString = "insert into [RMS].[dbo].[User_Role] values(:userId,:roleId)";
		Query createRoleQuery = getSession().createSQLQuery(createRoleQueryString.toString());
		createRoleQuery.setParameter("userId",userId);
		createRoleQuery.setParameter("roleId",1);
		int addedrows = createRoleQuery.executeUpdate();
		if(addedrows == 1) 
		System.out.println("Role to the user : " + userName + " is successfully assgined");
	}

	private List getUserIdGivenUserName(String userName) {
		String queryString = "SELECT [User_ID] FROM [RMS].[dbo].[User] where Username=:username";
		Query query = getSession().createSQLQuery(queryString.toString());
		query.setParameter("username", userName);
		List rows = query.list();
		return rows;
	}

	@Override
	public List<Map<String, Object>> getExistinglogins() {
		// TODO Auto-generated method stub
		List<Map<String,Object>> users = new ArrayList<Map<String,Object>>();
		String getUsersQuery = "SELECT users.[Username],users.[First_Name],users.[Last_Name],users.[Password] FROM [RMS].[dbo].[User] users,[RMS].[dbo].[User_Role] roles where users.[User_ID] = roles.[User_ID] and roles.[Role_ID] = 1";
		Query query = getSession().createSQLQuery(getUsersQuery.toString());
		List<Object[]> rows = query.list();
		for(Object[] row : rows){
			Map<String, Object> user = new HashMap<>();
			user.put("username", row[0].toString());
			user.put("firstname", row[1].toString());
			user.put("lastname", row[2].toString());
			user.put("password", row[3].toString());
			users.add(user);
		}
		return users;
	}
}

